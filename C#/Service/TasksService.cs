using System;
using TaskList.Model;
using System.Collections.Generic;
namespace TaskList.Service 
{
    public sealed class TasksService 
    {
        private TasksService()
        {
            this.tasks = new Dictionary<string, Task>();
        }
        private static TasksService instance;
        
        private Dictionary<string, Task> tasks;


        public void AddNewTask(Task task)
        {
            Task newTask = new Task.Builder()
                                    .WithId(System.Guid.NewGuid().ToString())
                                    .FromTask(task)
                                    .Build();
            this.tasks.Add(newTask.Id, newTask);
        }

        public Task findOne(string id) {
            return this.tasks[id];
        }

        public List<Task> GetAllTasks()
        {
            return new List<Task>(this.tasks.Values);
        }

        public void updateTask(string id , Task task) 
        {
            this.tasks[id] = task;
        }

        public void DeleteTask(string id) {
            this.tasks.Remove(id);
        }

        public static TasksService GetInstance()
        {
            if ( instance == null )
            {
                instance = new TasksService();
            }
            return instance;
        }

    }
}
