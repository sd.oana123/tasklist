using System;
using System.Collections.Generic;
using System.Linq;
using TaskList.Model;
using TaskList.Service;
using TaskList.Utils;
using Microsoft.AspNetCore.Mvc;

namespace TaskList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private TasksService tasksService = TasksService.GetInstance();

        // GET api/values
        [HttpGet]
        public ActionResult<List<Task>> Get()
        {
            return this.tasksService.GetAllTasks();
        }

        // GET api/tasks/5
        [HttpGet("{id}")]
        public Task Get(string id)
        {
            var task = this.tasksService.findOne(id);
            
            return task;
        }

        // POST api/tasks
        [HttpPost]
        public void Post([FromBody] Task value)
        {
            if(value.Name == null) {
                throw new Exception("You can't create a task with Null name");
            }
            this.tasksService.AddNewTask(value);
        }

        // PUT api/tasks/5
        [HttpPut("{id}")]
        public Task Put(string id, [FromBody] Task value)
        {
            this.tasksService.updateTask(id , value);
            return value;
        }

        // DELETE api/tasks/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            this.tasksService.DeleteTask(id);
        }
    }
}
