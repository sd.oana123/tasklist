﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TaskList.Model;

namespace TaskList
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
            Cutie a = new Cutie();
            a.put<Task>(new Task.Builder().Build());
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }

    public class Cutie
    {
        protected object obj;
        public object Get()
        {
            return this.obj;
        }

        public void keepCold() {

        }

        public void put<T>(T obj)
         {

         }
    }

    // public class CutieTaskList : Cutie<Task, List<Task>>
    // {
    //     public override void put(Task obj)
    //     {
    //         this.obj = new List<Task>();
    //         this.obj.Add(obj);
    //     }
    // }
}
