namespace TaskList.Utils
{

    public enum PriorityEnum
    {
        PRIORITY_HIGH, PRIORITY_MEDIUM, PRIORITY_LOW
    }
}