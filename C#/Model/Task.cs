using TaskList.Utils;
namespace TaskList.Model
{

    public sealed class Task
    {
        public Task(string name, string description, string date, PriorityEnum priority)
        {
            this.name = name;
            this.description = description;
            this.date = date;
            this.priority = priority;
        }

        private Task(string id, string name, string description, string date, PriorityEnum priority)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.date = date;
            this.priority = priority;
        }

        private string id;

        private string name;

        private string description;

        private string date;

        private PriorityEnum priority;

        public string Id { get => id; }
        public string Name { get => name; }
        public string Description { get => description;  }
        public string Date { get => date;  }
        public PriorityEnum Priority { get => priority;  }

        public class Builder
        {
            private string id = null;

            private string name;

            private string description;

            private string date;

            private PriorityEnum priority;

            public Builder WithId(string id)
            {
                this.id = id;
                return this;
            }

            public Builder WithName(string name) {
                this.name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                this.description = description;
                return this;
            }

            public Builder WithDate(string date)
            {
                this.date = date;
                return this;
            }

            public Builder WithPriority(PriorityEnum priority)
            {
                this.priority = priority;
                return this;
            }

            public Builder FromTask(Task task)
            {
                this.date = task.date;
                this.name = task.name;
                this.description = task.description;
                this.priority = task.priority;
                return this;
            }

            public Task Build() 
            {
                return new Task(id, name, description, date, priority);
            }
        }
    }

}